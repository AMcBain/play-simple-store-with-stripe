This is an application using the Play framework (1.2.5) for a simple store using
Stripe ( https://stripe.com/ ) as a payment processor.

This sample store sells a maximum of 3 items per person. People can optionally
choose to donate to cover costs and whether or not they want a tung oil finish
on the item. Obviously for your own store, you will want to change this.

Despite the optional payment being listed as a donation, unless you have tax-
exempt status, those in the US may still need to collect taxes depending on the
laws for internet businesses from the states in which donating customers reside.
Those in Canada would need to do the same for provinces. This application as it
stands DOES NOT calculate or collect taxes. So if needed, don't forget to modify
it appropriately be properly lawful.

If you deploy this application for real use, it should be over HTTPS! DO NOT
risk your customers' data over an unsecured connection. There are plenty of
places offering store-trusted certificates, please acquire one.

This store requires JavaScript to work (which displays a message if the user has
turned if off in the browser). As a result of this, a customer's payment
information (credit card, CVC/CVV2, expiration, and billing address) is not
stored on the server. The server is only a pass-through of this encrypted data
to Stripe. The store does save the amount donated, quantity ordered,
(optionally) an email address, finish, and their shipping address.

The icons used in this application are from the Fugue icon set created by Yusuke
Kamiyamane and available at http://p.yusukekamiyamane.com/

The idyllic water header image is mine and I give rights to its use, however,
you will probably want to change it to something that more suits your particular
store and what you are selling in it.

The rest is licensed under the BSD 3-clause license, a copy of which is included
with the source. http://opensource.org/licenses/BSD-3-Clause


**Setup instructions:**

Before deploying, some setup needs to be done to make the application ready to
run. Anything starting with "run" needs to be done from a command line in the
application's directory.

1. run "play secret"

     _this generates a new secret key for the application. by default, the
     application does not ship with one._

2. run "play dependencies"

     _This connects to the internet to download all the required dependencies._

3. Edit the application.conf directory to set up OpenID, Stripe, email, etc. and
   follow the instructions or notes above each entry. Items to edit are below:

     1. application.name

     2. application.defaultOpenId

     3. application.openIdSalt

     4. application.stripeApiKey

        _This requires creating and setting up an account with Stripe. They
        do have a test-API key you can use until then, however._

     5. application.mode

        _You will want to set this to "prod" on live deployment. When
        testing, leave it as "dev". If the framework you use for
        deployment is different from your testing framework, you can use
        the "%frameworkid.application.mode" syntax to automatically switch._

     6. db

        _Any of the "db" properities. There is no default database so the
        app will fail to start if you don't select one. Don't use "db=mem"
        for a long-term deployed application._

     7. mail

        _If you want actual mail to be sent out when users are charged or
        their order ships, these need to be filled out. The default of
        "mock" just prints to the console._

     8. Set up any admin user[s] in the config/initial-data.yml file

        1. The OpenID handling automatically creates users on the fly when
           they log in to the application if that user doesn't already exist
           in the system. By pre-defining users to be loaded at the start of
           the application, their admin flag can be set before a non-admin
           user is created for them when they log in.

        2. Only pre-define admin users if you trust the end providing OpenID.
           Anyone who can gain access to an OpenIDable account pre-marked
           with the admin flag will have extra access. Within this app admins
           can only view all orders and ship them, but even so the caution
           applies. If you do not trust the OpenID provider but must work
           with them, you need to set up another way to allow admins access
           separate from the OpenID users.

     9. Quantity

        _This requires some code hacking. The default quantity is set to 50
        and because of the simplicity of this application is hard-coded in
        to the app/Application.java file. Just change the two spots 50 is
        used to update the quantity, or complexify this application with a
        better way to update the quantity on the fly as more stock becomes
        available._

     10. Stripe Webhooks (callbacks)

        _Stripe supports calling web applications back after transactions to
        let them know the results. This app has a route `/stripe` that points
        to the `Application.stripe` action which listens for successful charges
        and marks the appropriate orders as such. This is an optional step but
        you may find it to be a useful feature. To set this up as a webhook,
        follow the instructions below._

        1. Log in to your Stripe account.

        2. Click the account dropdown at the top right and go to "Account Settings".

        3. Select the Webhooks tab.

        4. Click "Add URL", paste in the URL to your app's `/stripe` route, and
           click "Create endpoint".

        _The app currently only looks for `charge.succeeded` events so you can tell
        Stripe to only send those events when setting up the webhook, but it won't
        hurt anything if you tell Stripe to send all events. Getting all events
        would make it easier to add support for others in the future, such as
        `charge.failed`_
