Stripe.setPublishableKey('SET Stripe PUBLIC API KEY HERE');

(function() {
    function stripeResponseHandler(status, response) {

        if (response.error) {
            $('button').removeAttr('disabled');
            $('#error').html(response.error.message);
            location.hash = "#error";
        }
        else {
            var form = $('form');
            form.append('<input type="hidden" name="stripeToken" value="' + response['id'] + '">');

            $('[name^="shipping"]').each(function() {
                $(this).removeAttr('disabled');
            });

            form[0].submit();
        }
    }

    function disabled(item, disabled) {
        item = $(item);
        return (disabled ? item.attr('disabled', 'disabled') : item.removeAttr('disabled'));
    }

    $(document).ready(function() {
        var sameaddr = $('[name="sameaddr"]'), sameaddrDefaultChecked = !!sameaddr.attr('checked');

        var nodonation = $('[name="order.nodonation"]');
        nodonation.change(function() {
            disabled('[name="order.amount"]', this.checked);
            disabled('[name="order.currency"]', this.checked);
            disabled('[data-name^="card"]', this.checked);
            disabled('[data-name^="billing"]', this.checked);
            disabled('[name="sameaddr"]', this.checked).removeAttr('checked');
            disabled('[name^="shipping"]', false);
        }).change();

        if (sameaddrDefaultChecked) {
            $('[name^="shipping"]').each(function() {
                $('[data-name="billing.' + this.getAttribute('name').split('.')[1] + '"]').val(this.value);
            });
            sameaddr.attr('checked', 'checked');
        }
        sameaddr.change(function() {
            disabled('[name^="shipping"]', this.checked);

            if (this.checked) {
                $('[data-name^="billing"]').each(function() {
                    $('[name="shipping.' + this.getAttribute('data-name').split('.')[1] + '"]').val(this.value);
                });
            }
        }).change();

        $("form").submit(function(event) {

            if (sameaddr[0].checked) {
                $('[data-name^="billing"]').each(function() {
                    $('[name="shipping.' + this.getAttribute('data-name').split('.')[1] + '"]').val(this.value);
                });
            }

            if (!nodonation[0].checked) {
                $('button').attr('disabled', 'disabled');

                Stripe.createToken({
                    number: $('[data-name="card.number"]').val(),
                    cvc: $('[data-name="card.cvc"]').val(),
                    exp_month: $('[data-name="card.expiremonth"]').val(),
                    exp_year: $('[data-name="card.expireyear"]').val(),
                    name: $('[data-name="billing.name"]').val(),
                    address_line1: $('[data-name="billing.line1"]').val(),
                    address_line2: $('[data-name="billing.line2"]').val(),
                    address_city: $('[data-name="billing.city"]').val(),
                    address_state: $('[data-name="billing.state"]').val(),
                    address_zip: $('[data-name="billing.zip"]').val(),
                    address_country: $('[data-name="billing.country"]').val(),
                }, stripeResponseHandler);
                return false;
            }
        });
        $('button').removeAttr('disabled');
    });
}());
