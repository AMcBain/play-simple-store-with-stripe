package jobs;

import play.jobs.*;

import models.*;

import notifiers.Email;

public class Receipt extends Job {

    private Order order;

    public Receipt(Order order) {
        this.order = order;
    }

    public void doJob() {
        Email.receipt(order);
    }
}
