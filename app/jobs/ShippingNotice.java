package jobs;

import play.jobs.*;

import models.*;

import notifiers.Email;

public class ShippingNotice extends Job {

    private Order order;

    public ShippingNotice(Order order) {
        this.order = order;
    }

    public void doJob() {
        Email.shipped(order);
    }
}
