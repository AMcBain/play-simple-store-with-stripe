package controllers;

import play.*;
import play.mvc.*;
import play.data.validation.*;

import java.util.*;
import java.math.BigDecimal;
import javax.persistence.Query;

import models.*;
import jobs.*;

import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.*;

import org.yaml.snakeyaml.Yaml;

public class Application extends Controller {

    private static final BigDecimal ONE_HUNDRED = new BigDecimal(100);

    static {
        Stripe.apiKey = Play.configuration.getProperty("application.stripeApiKey", "");
    }

    @Before(unless={"signin","stripe"})
    public static void check() {

        User user = Security.getSessionUser();
        renderArgs.put("user", user);

        if (user == null) {
           signin();
        }
    }

    public static void signin() {

        if (Security.getSessionUser() == null) {
            render();
        }
        store();
    }

    // Move into User or Order?
    private static long userOrderQuantity() {
        Query query = Order.em().createQuery("select coalesce(sum(quantity),0) from Order1 where user = ?");
        return (Long)query.setParameter(1, renderArgs.get("user")).getSingleResult();
    }

    private static long totalOrderQuantity() {
        Query query = Order.em().createQuery("select coalesce(sum(quantity),0) from Order1");
        return (Long)query.getSingleResult();
    }

    public static void store() {

        if (userOrderQuantity() > 2) {
            flash.keep();
            orders(false);
        }
        else if (totalOrderQuantity() == 50) {
            flash.put("error", "Sorry, no more in stock");
            flash.keep();
            orders(false);
        }
        render();
    }

    public static void order(@Valid Order order, @Valid Address shipping, String stripeToken) {
        checkAuthenticity();

        if (totalOrderQuantity() + order.quantity > 50) {
            Validation.addError("order.quantity1", "Not enough in stock");
        }

        long quantity = userOrderQuantity();
        if (quantity > 3 || quantity + order.quantity > 3) {
            Validation.addError("order.quantity", "Limit 3 per person");
        }

        if (!Validation.hasError("order.amount") && !order.nodonation && !"USD".equals(order.currency)) {
            Validation.addError("order.amount", "Currency must be USD.");
        }

        if(validation.hasErrors()) {
            params.flash();
            validation.keep();
        }
        else {
            User user = (User)renderArgs.get("user");

            order.user = user;
            order.address = shipping;
            order.submitted = new Date();

            if (order.nodonation) {
                order.amount = BigDecimal.ZERO;
                order.currency = null;
                order.save();
                new Receipt(order).now();
                flash.put("success", "Thank you for your order.");
            }
            else {
                Map<String, Object> chargeParams = new HashMap<String, Object>();
                chargeParams.put("description", Play.configuration.getProperty("application.name", "AppName") +
                        " - Order for " + user.name + ": " + order.quantity + " keyfob; tungoil? " + order.tungoil);
                chargeParams.put("amount", order.amount.multiply(ONE_HUNDRED).intValue());
                chargeParams.put("currency", order.currency);
                chargeParams.put("card", stripeToken);

                try {
                    Charge charge = Charge.create(chargeParams);

                    String error = charge.getFailureMessage();
                    if (error != null) {
                        Validation.addError("stripe", error);
                    }

                    Card card = charge.getCard();
                    if ("fail".equals(card.getCvcCheck())) {
                        Validation.addError("card.cvc", "The CVC provided is incorrect.");
                    }                    
                    if ("fail".equals(card.getAddressLine1Check())) {
                        Validation.addError("billing.line1", "Incorrect.");
                    }
                    if ("fail".equals(card.getAddressZipCheck())) {
                        Validation.addError("billing.zip", "Incorrect.");
                    }

                    if (validation.hasErrors()) {
                        params.flash();
                        validation.keep();
                    }
                    else {
                        order.chargeId = charge.getId();
                        order.save();
                        flash.put("success", "Thank you for your order.");
                    }
                }
                catch(Exception e) {
                    Validation.addError("stripe", e.getMessage());
                    params.flash();
                    validation.keep();
                }
            }
        }
        store();
    }

    public static void orders(boolean sent) {
        List<Order> orders = null;

        User user = (User)renderArgs.get("user");
        if (user != null) {

            String query = "order by submitted desc";
            if (sent) {
                query = "shipped is not null " + query;
            }

            if(user.isAdmin) {
                orders = Order.find(query).fetch();
            }
            else {
                orders = Order.find("user = ?" + query, user).fetch();
            }
        }
        render(orders);
    }

    public static void ship(Long id) {

        User user = (User)renderArgs.get("user");
        if(user.isAdmin) {

            Order order = Order.find("byId", id).first();
            if (order.shipped == null) {

                order.shipped = new Date();
                order.save();
                new ShippingNotice(order).now();
            }
            orders(false);
        }
        notFound();
    }

    public static void stripe() throws StripeException {
        Map<Object, Object> data = (Map<Object, Object>)new Yaml().load(request.body);
        Event event = Event.retrieve((String)data.get("id"));

        if ("charge.succeeded".equals(event.getType())) {

            Charge charge = (Charge)event.getData().getObject();
            Order order = Order.find("byChargeId", charge.getId()).first();

            if (order.charged == null) {
                order.charged = new Date();
                order.save();
                new Receipt(order).now();
            }
        }
    }
}
