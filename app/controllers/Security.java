package controllers;

import play.Play;
import play.Logger;
import play.cache.Cache;
import play.libs.Crypto;
import play.mvc.*;

import patches.OpenID;
import patches.OpenID.UserInfo;

import models.User;

public class Security extends Controller {

    public static final String USER_KEY = "user";
    private static final String NONCE_KEY_PREFIX = "nonce:";

    private static final int OPENID_TIMEOUT_SECONDS;
    private static final int OPENID_TIMEOUT_MILLIS;
    static {
        String value = Play.configuration.getProperty("application.openIdTimeout", "");
        int timeout = 15;

        if (!value.isEmpty()) {
            try {
                timeout = Integer.parseInt(value);
            }
            catch (NumberFormatException e) {
                Logger.warn("Invalid openIdTimeout: '" + value + "'; using " + timeout);
            }
        }
        OPENID_TIMEOUT_SECONDS = timeout;
        OPENID_TIMEOUT_MILLIS = timeout * 1000;
    }

    public static User getSessionUser() {
        return User.find("byName", session.get(USER_KEY)).first();
    }

    private static String sign(String value) {
        return Crypto.sign(value + Play.configuration.getProperty("application.openIdSalt", ""));
    }

    public static void authOpenID(String nonce) {

        if(OpenID.isAuthenticationResponse()) {
            UserInfo verifiedUser = OpenID.getVerifiedID();

            try {
                String[] bits = nonce.split(":");
                long time = Long.parseLong(bits[0]);
                long now = System.currentTimeMillis();

                if (Cache.get(NONCE_KEY_PREFIX + time) == null && bits[1].equals(sign(bits[0])) &&
                        now - time <= OPENID_TIMEOUT_MILLIS) {

                    Cache.set(NONCE_KEY_PREFIX + time, Boolean.TRUE, OPENID_TIMEOUT_SECONDS + "s");
                }
                else {
                    nonce = null;
                }
            }
            catch (Exception e) {
                nonce = null;
            }

            if(nonce == null || verifiedUser == null || !verifiedUser.extensions.containsKey("nickname")) {
                flash.put("openid.error", "Authentication failed");
            }
            else {
                String nickname = verifiedUser.extensions.get("nickname");

                User user = User.find("byName", nickname).first();
                if (user == null) {
                    user = new User();
                    user.name = verifiedUser.extensions.get("nickname");
                    user.save();
                }
                session.put(USER_KEY, nickname);
            }
        }
        else {
            OpenID id = OpenID.id(Play.configuration.getProperty("application.defaultOpenId", ""))
                    .required("nickname");

            String returnTo = Play.configuration.getProperty("application.baseUrl", "");
            if (returnTo.endsWith("/")) {
                returnTo = returnTo.substring(0, returnTo.length() - 1);
            }

            // Router.getFullUrl (and in turn, the protected getBaseUrl) doesn't respect
            //  the baseUrl property when a valid request is present, like now ...
            if (returnTo.isEmpty()) {
                returnTo = Router.getFullUrl(request.action);
            } else {
                Router.ActionDefinition def = Router.reverse(request.action);
                returnTo += def.url;
            }
            long time = System.currentTimeMillis();
            id = id.forRealm(returnTo).returnTo(returnTo + "/" + time + ":" + sign(time + ""));

            // Nectarine doesn't care that LaserStoreApp is not a valid user, they'll return us
            // the username of anyone who signs in (or is signed in) when we send our request.
            // OpenID has a param to require the username exist/match, but we don't want that.
            if(!id.verify()) {
                flash.put("openid.error", "Cannot verify your OpenID");
            } 
        }
        redirect("/");
    }

}
