package notifiers;
 
import play.*;
import play.mvc.*;
import java.util.*;

import models.*;

import com.stripe.model.Charge;
 
public class Email extends Mailer {

    public static String setup(Order order, String subject) {
        setFrom(Play.configuration.get("mail.addressName") + " <" + Play.configuration.get("mail.address") + ">");
        addRecipient(order.user.name + " <" + order.email + ">");
        setSubject(subject, Play.configuration.get("application.name"));
        return null;
    }

    public static void receipt(Order order) {
        if (order.email != null && !order.email.isEmpty()) {
            setup(order, "Receipt for your payment to %1$s");
            send(order);
        }
   }

   public static void shipped(Order order) {
        if (order.email != null && !order.email.isEmpty()) {
            setup(order, "Shipping notice from %1$s");
            send(order);
        }
   }
}
