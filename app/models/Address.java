package models;

import java.util.*;

import javax.persistence.*;

import play.db.jpa.*;
import play.data.validation.*;
 
@Entity
public class Address extends Model {

    public String name;

    @Required
    public String line1;

    public String line2;

    @Required
    public String city;

    // Not required because non-US addresses might not have a state/province equivalent to put here.
    public String state;

    @Required
    public String zip;

    @Required
    public String country;

    public String toString() {
        return "Address(name=" + name + ", line1=" + line1 + ", line2=" + line2 + ", city=" + city +
               ", state=" + state + ", zip=" + zip + ", country=" + country + ")";
    }

}
