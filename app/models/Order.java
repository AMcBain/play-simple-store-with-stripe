package models;

import java.util.*;
import java.math.BigDecimal;
import javax.persistence.*;

import play.db.jpa.*;
import play.data.validation.*;
 
@Entity(name="Order1")
public class Order extends Model {

    @OneToOne
    public User user;

    public Date submitted;

    public String chargeId;

    public Date charged;

    public Date shipped;

    @Required
    public boolean nodonation;

    @CheckWith(value=AmountNoDonationCheck.class, message="Please enter an amount or select 'do not wish to donate'")
    public BigDecimal amount;

    @CheckWith(value=RequiredIfDonationCheck.class, message="Currency required if making a donation")
    public String currency;

    @Email(message="Email address is not valid")
    public String email;

    @Required
    @Min(value=1, message="Please enter a valid quantity (1 or 2)")
    @Max(value=2, message="Please enter a valid quantity (1 or 2)")
    public int quantity;

    @Required
    public boolean tungoil;

    @OneToOne @MapsId
    public Address address;

    public String trackingUrl;

    public String toString() {
        return "Order(user=" + user + ", submitted=" + submitted + ", charged=" + charged + ", chargedId=" + chargeId +
               ", shipped=" + shipped + ", amount=" + amount + ", currency=" + currency +", email=" + email +
               ", quantity=" + quantity + ", tungoil=" + tungoil + ", address=" + address + ")";
    }

    private static class RequiredIfDonationCheck extends Check {

        public boolean isSatisfied(Object validatedObject, Object value) {
            Order order = (Order)validatedObject;
            return (order.nodonation || order.currency != null);
        }
    }

    private static class AmountNoDonationCheck extends Check {

        public boolean isSatisfied(Object validatedObject, Object value) {
            Order order = (Order)validatedObject;
            return (order.nodonation || order.amount.compareTo(BigDecimal.ZERO) == 1);
        }
    }

}
