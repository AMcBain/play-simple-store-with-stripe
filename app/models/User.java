package models;

import java.util.*;

import javax.persistence.*;

import play.db.jpa.*;
import play.data.validation.*;
 
@Entity
public class User extends Model {

    @Required
    public String name;

    @Required
    public boolean isAdmin;

    @OneToMany(mappedBy="user", cascade=CascadeType.ALL)
    public List<Order> order;

    public String toString() {
        return name;
    }

}
